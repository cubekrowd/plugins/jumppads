package net.cubekrowd.jumppads;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class DeleteCommand implements Subcommand {
    private final JumpPadsPlugin plugin;

    public DeleteCommand(JumpPadsPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    @Override
    public String getName() {
        return "delete";
    }

    @Override
    public String getUsage() {
        return "<name>";
    }

    @Override
    public String getDescription() {
        return "Deletes a jump pad type";
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Please specify a jump pad type.");
            return;
        }

        var deleted = plugin.deleteJumpPadType(args[0]);

        if (deleted == null) {
            // no matching jump pad type was found
            sender.sendMessage(ChatColor.RED + "Unknown jump pad type \"" + args[0] + "\".");
        } else {
            sender.sendMessage(JumpPadsPlugin.MAIN_COLOUR + "Deleted jump pad type \""
                    + deleted.getName() + "\".");
        }
    }

    @Override
    public List<String> getCompletions(CommandSender sender, String[] args) {
        if (args.length == 1) {
            return plugin.getJumpPadTypeCompletions(args[0]);
        }

        return Collections.emptyList();
    }
}

package net.cubekrowd.jumppads;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.StringJoiner;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class TypeCommand implements Subcommand {
    private final JumpPadsPlugin plugin;
    private final List<TypeCommand.Subcommand> subcommands;

    public TypeCommand(JumpPadsPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
        subcommands = List.of(
                new TypeInfoCommand(plugin),
                new TypeHeightCommand(plugin),
                new TypeDistanceCommand(plugin));
    }

    public interface Subcommand {
        String getName();

        String getUsage();

        String getDescription();

        void execute(CommandSender sender, JumpPadType type, String[] args);
    }

    @Override
    public String getName() {
        return "type";
    }

    @Override
    public String getUsage() {
        return "<name> ...";
    }

    @Override
    public String getDescription() {
        return "Edit a jump pad type.";
    }

    private void sendTypeHelp(CommandSender sender) {
        var joiner = new StringJoiner("\n");
        joiner.add(ChatColor.GRAY + "[" + JumpPadsPlugin.MAIN_COLOUR + plugin.getName() + ChatColor.GRAY + "] " + ChatColor.WHITE + "Type subcommands:");

        for (var cmd : subcommands) {
            var usage = cmd.getUsage() == null ? "" : " " + cmd.getUsage();

            joiner.add(ChatColor.GRAY + "- " + cmd.getName() + usage
                    + ChatColor.WHITE + " - " + cmd.getDescription());
        }

        sender.sendMessage(joiner.toString());
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Please enter a jump pad type.");
            return;
        }

        var selected = plugin.getJumpPadType(args[0]);

        if (selected == null) {
            sender.sendMessage(ChatColor.RED + "There is no such jump pad type.");
            return;
        }

        if (args.length == 1) {
            sendTypeHelp(sender);
        } else {
            var arg = args[1].toLowerCase(Locale.ENGLISH);

            for (var cmd : subcommands) {
                if (cmd.getName().equals(arg)) {
                    cmd.execute(sender, selected, Arrays.copyOfRange(args, 2, args.length));
                    return;
                }
            }

            // no matching subcommand, so just send help
            sendTypeHelp(sender);
        }
    }

    @Override
    public List<String> getCompletions(CommandSender sender, String[] args) {
        // Note: number of arguments is always at least 1
        if (args.length == 1) {
            return plugin.getJumpPadTypeCompletions(args[0]);
        } else if (args.length == 2) {
            var arg = args[1].toLowerCase(Locale.ENGLISH);
            var result = new ArrayList<String>();

            for (var cmd : subcommands) {
                if (cmd.getName().startsWith(arg)) {
                    result.add(cmd.getName());
                }
            }
            return result;
        } else {
            return Collections.emptyList();
        }
    }
}

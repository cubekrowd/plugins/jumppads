package net.cubekrowd.jumppads;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class HelpCommand implements Subcommand {
    private final JumpPadsPlugin plugin;

    public HelpCommand(JumpPadsPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getUsage() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Displays the help page.";
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        var joiner = new StringJoiner("\n");
        joiner.add(ChatColor.GRAY + "[" + JumpPadsPlugin.MAIN_COLOUR + plugin.getName() + ChatColor.GRAY + "] " + ChatColor.WHITE + "Main subcommands:");

        for (var cmd : plugin.getSubcommands()) {
            var usage = cmd.getUsage() == null ? "" : " " + cmd.getUsage();

            joiner.add(ChatColor.GRAY + "- " + cmd.getName() + usage
                    + ChatColor.WHITE + " - " + cmd.getDescription());
        }

        sender.sendMessage(joiner.toString());
    }

    @Override
    public List<String> getCompletions(CommandSender sender, String[] args) {
        return Collections.emptyList();
    }
}

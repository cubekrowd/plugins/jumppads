package net.cubekrowd.jumppads;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class ListCommand implements Subcommand {
    private final JumpPadsPlugin plugin;

    public ListCommand(JumpPadsPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    @Override
    public String getName() {
        return "list";
    }

    @Override
    public String getUsage() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Shows the list of jump pad types.";
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        var types = plugin.getJumpPadTypes();

        if (types == null || types.size() == 0) {
            sender.sendMessage(JumpPadsPlugin.MAIN_COLOUR + "There are no jump pad types currently.");
            return;
        }

        var joiner = new StringJoiner("\n");
        joiner.add(ChatColor.GRAY + "[" + JumpPadsPlugin.MAIN_COLOUR + plugin.getName() + ChatColor.GRAY + "] "
                + ChatColor.WHITE + "Jump pad types:");

        for (var type : types) {
            joiner.add(ChatColor.GRAY + "- " + type.getName() + ChatColor.WHITE + " (" + type.getBlockType().getKey().getKey() + ")");
        }

        sender.sendMessage(joiner.toString());
    }

    @Override
    public List<String> getCompletions(CommandSender sender, String[] args) {
        return Collections.emptyList();
    }
}

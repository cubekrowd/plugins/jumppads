package net.cubekrowd.jumppads;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.SoundCategory;
import org.bukkit.Tag;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;

public final class JumpPadsPlugin extends JavaPlugin implements Listener {
    public static final ChatColor MAIN_COLOUR = ChatColor.DARK_GREEN;
    private final Collection<JumpPadType> jumpPadTypes = new ArrayList<>();
    private HelpCommand helpCommand;
    private List<Subcommand> subcommands;

    public List<Subcommand> getSubcommands() {
        return subcommands;
    }

    public Collection<JumpPadType> getJumpPadTypes() {
        return jumpPadTypes;
    }

    public JumpPadType createJumpPadType(String name, Material blockType) {
        var res = new JumpPadType(name, blockType);
        jumpPadTypes.add(res);

        getConfig().set(name + ".block-type", blockType.getKey().toString());
        getConfig().set(name + ".distance", 0);
        getConfig().set(name + ".height", 0);
        saveConfig();
        return res;
    }

    public JumpPadType getJumpPadType(String name) {
        var lowerName = name.toLowerCase(Locale.ENGLISH);

        for (var type : getJumpPadTypes()) {
            if (type.getName().equals(lowerName)) {
                return type;
            }
        }

        // no match found
        return null;
    }

    public JumpPadType deleteJumpPadType(String name) {
        var lowerName = name.toLowerCase(Locale.ENGLISH);
        var typeIter = jumpPadTypes.iterator();

        while (typeIter.hasNext()) {
            var type = typeIter.next();

            if (type.getName().equals(lowerName)) {
                typeIter.remove();
                getConfig().set(name, null);
                saveConfig();
                return type;
            }
        }

        // no matching jump pad type found
        return null;
    }

    public List<String> getJumpPadTypeCompletions(String partialName) {
        var prefix = partialName.toLowerCase(Locale.ENGLISH);
        var result = new ArrayList<String>(jumpPadTypes.size());

        for (var type : jumpPadTypes) {
            var name = type.getName();
            if (name.startsWith(prefix)) {
                result.add(name);
            }
        }

        // sort with the natural order of strings, i.e. lexicographical order
        result.sort(null);
        return result;
    }

    @Override
    public void onEnable() {
        getConfig().options().copyDefaults(true);
		saveConfig();
		for (var name : getConfig().getKeys(false)) {
		    var section = getConfig().getConfigurationSection(name);
		    if (section == null) {
		        continue;
            }
		    var blockTypeName = section.getString("block-type");
		    var blockType = Material.matchMaterial(blockTypeName);
		    if (blockType == null) {
		        getLogger().warning("Skipping jump pad type " + name + ", because block type " + blockTypeName + " is invalid");
		        continue;
            }
		    var distance = section.getDouble("distance");
		    var height = section.getDouble("height");
		    jumpPadTypes.add(new JumpPadType(name.toLowerCase(Locale.ENGLISH), blockType, distance, height));
        }
        getServer().getPluginManager().registerEvents(this, this);

		helpCommand = new HelpCommand(this);
		subcommands = List.of(
		        helpCommand,
                new CreateCommand(this),
                new DeleteCommand(this),
                new ListCommand(this),
                new TypeCommand(this),
                new AboutCommand(this));
    }

    @Override
    public void onDisable() {
        jumpPadTypes.clear();
        helpCommand = null;
        subcommands = null;
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerMove(PlayerMoveEvent e) {
        var p = e.getPlayer();

        // NOTE(cag): Don't bounce spectators
        if (p.getGameMode() == GameMode.SPECTATOR) {
            return;
        }

        var padBlock = e.getTo().getBlock();
        var from = e.getFrom().getBlock();

        // NOTE(traks): only do the launch once. For players with high ping the
        // velocity keeps being applied for a while, even though the client is
        // already simulating the initial velocity change and is somewhere
        // flying through the air.
        if (from.equals(padBlock)) {
            return;
        }
        if (!Tag.PRESSURE_PLATES.isTagged(padBlock.getType())) {
            return;
        }

        var padType = e.getTo().getBlock().getRelative(0, -2, 0).getType();
        var selected = jumpPadTypes.stream().filter(jpt -> jpt.getBlockType() == padType).findFirst().orElse(null);
        if (selected == null) {
            return;
        }
        var loc = p.getLocation();
        var vel = loc.getDirection().multiply(selected.getDistance()).setY(selected.getHeight());
        p.setVelocity(vel);
        p.playSound(loc, Sound.ENTITY_ENDER_DRAGON_FLAP, SoundCategory.HOSTILE, 0.5f, 1);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (!sender.hasPermission("jumppads.configure")) {
            sender.sendMessage(ChatColor.RED + "You are not allowed to use this command.");
            return true;
        }
        if (args.length == 0) {
            helpCommand.execute(sender);
        } else {
            var arg = args[0].toLowerCase(Locale.ENGLISH);
            Subcommand targetCmd = helpCommand;

            for (var cmd : subcommands) {
                if (cmd.getName().equals(arg)) {
                    targetCmd = cmd;
                    break;
                }
            }

            targetCmd.execute(sender, Arrays.copyOfRange(args, 1, args.length));
        }
        return true;
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command command,
            String alias, String[] args) {
        if (!sender.hasPermission("jumppads.configure")) {
            return Collections.emptyList();
        }

        // Note: number of arguments is always at least 1
        var arg = args[0].toLowerCase(Locale.ENGLISH);

        if (args.length == 1) {
            var result = new ArrayList<String>();

            for (var cmd : subcommands) {
                if (cmd.getName().startsWith(arg)) {
                    result.add(cmd.getName());
                }
            }
            return result;
        } else {
            for (var cmd : subcommands) {
                if (cmd.getName().equals(arg)) {
                    return cmd.getCompletions(sender, Arrays.copyOfRange(args, 1, args.length));
                }
            }
            return Collections.emptyList();
        }
    }
}

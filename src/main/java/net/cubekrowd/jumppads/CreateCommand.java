package net.cubekrowd.jumppads;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;

public final class CreateCommand implements Subcommand {
    private final JumpPadsPlugin plugin;

    public CreateCommand(JumpPadsPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    @Override
    public String getName() {
        return "create";
    }

    @Override
    public String getUsage() {
        return "<name> <type>";
    }

    @Override
    public String getDescription() {
        return "Creates a jump pad type with the name and block type.";
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if (args.length < 2) {
            sender.sendMessage(ChatColor.RED + "Please specify a name and block type.");
            return;
        }

        var name = args[0];
        var blockType = Material.matchMaterial(args[1]);

        if (blockType == null || !blockType.isBlock()) {
            sender.sendMessage(ChatColor.RED + "That is not a block type.");
            return;
        }

        var lowerName = name.toLowerCase(Locale.ENGLISH);

        for (var padType : plugin.getJumpPadTypes()) {
            if (padType.getName().equals(lowerName)) {
                sender.sendMessage(ChatColor.RED + "The name \"" + lowerName + "\" is already in use.");
                return;
            }
        }

        plugin.createJumpPadType(lowerName, blockType);
        sender.sendMessage(JumpPadsPlugin.MAIN_COLOUR + "Created the jump pad type \"" + lowerName + "\"!");
    }

    @Override
    public List<String> getCompletions(CommandSender sender, String[] args) {
        return Collections.emptyList();
    }
}

package net.cubekrowd.jumppads;

import java.util.Objects;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class TypeHeightCommand implements TypeCommand.Subcommand {
    private final JumpPadsPlugin plugin;

    public TypeHeightCommand(JumpPadsPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    @Override
    public String getName() {
        return "height";
    }

    @Override
    public String getUsage() {
        return "<height>";
    }

    @Override
    public String getDescription() {
        return "Height of the jump.";
    }

    @Override
    public void execute(CommandSender sender, JumpPadType type, String[] args) {
        if (args.length < 1) {
            sender.sendMessage(ChatColor.RED + "Please enter the height.");
            return;
        }

        double newHeight;
        try {
            newHeight = Double.parseDouble(args[0]);
        } catch (NumberFormatException e) {
            sender.sendMessage(ChatColor.RED + "That is not a valid height.");
            return;
        }

        type.setHeight(newHeight);
        plugin.getConfig().set(type.getName() + ".height", newHeight);
        plugin.saveConfig();
        sender.sendMessage(JumpPadsPlugin.MAIN_COLOUR + "Set the jump height of \""
                + type.getName() + "\" to " + newHeight + ".");
    }
}

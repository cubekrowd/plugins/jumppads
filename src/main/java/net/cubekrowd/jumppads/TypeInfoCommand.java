package net.cubekrowd.jumppads;

import java.util.Objects;
import java.util.StringJoiner;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class TypeInfoCommand implements TypeCommand.Subcommand {
    private final JumpPadsPlugin plugin;

    public TypeInfoCommand(JumpPadsPlugin plugin) {
        this.plugin = Objects.requireNonNull(plugin, "plugin");
    }

    @Override
    public String getName() {
        return "info";
    }

    @Override
    public String getUsage() {
        return null;
    }

    @Override
    public String getDescription() {
        return "Displays jump pad type info.";
    }

    @Override
    public void execute(CommandSender sender, JumpPadType type, String[] args) {
        var joiner = new StringJoiner("\n");
        joiner.add(ChatColor.GRAY + "[" + JumpPadsPlugin.MAIN_COLOUR + plugin.getName() + ChatColor.GRAY + "] " + ChatColor.WHITE + "Jump pad type info:");
        joiner.add(ChatColor.GRAY + "- Name: " + ChatColor.WHITE + type.getName());
        joiner.add(ChatColor.GRAY + "- Block type: " + ChatColor.WHITE + type.getBlockType().getKey());
        joiner.add(ChatColor.GRAY + "- Distance: " + ChatColor.WHITE + type.getDistance());
        joiner.add(ChatColor.GRAY + "- Height: " + ChatColor.WHITE + type.getHeight());

        sender.sendMessage(joiner.toString());
    }
}
